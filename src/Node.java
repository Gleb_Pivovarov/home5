
import java.util.*;

public class Node {

   private String name;
   private Node firstChild;
   private Node nextSibling;

   Node (String n, Node d, Node r) {
      this.name = n;
      this.firstChild = d;
      this.nextSibling = r;
   }
   
   public static Node parsePostfix (String s) {
      if (s.contains("\t")) {
          throw new RuntimeException("tab char in postfix form " + s);
      } else if(s.contains(" ")) {
          throw new RuntimeException("space char in postfix form " + s);
      } else if(s.contains("()")) {
          throw new RuntimeException("empty subtree in postfix form " + s);
      } else if(s.contains(",,")) {
          throw new RuntimeException("double comas in postfix form " + s);
      } else if(s.contains("(,")) {
          throw new RuntimeException("node missing in postfix form " + s);
      } else if(s.contains("((") && s.contains("))")) {
          throw new RuntimeException("Root can't have siblings in postfix form " + s);
      }
       String[] ary = s.split("");
      Node node = new Node(null,null,null);
      Stack<Node> stack = new Stack();
      int start = 0;
      int end = 0;

      for (int i = 0; i < ary.length; i++){
          if (ary[i].equals("(")){
              start++;
              stack.push(node);
              node.firstChild = new Node(null, null, null);
              node = node.firstChild;
          }else if ((ary[i].equals(","))){
             if (start == end) throw new RuntimeException("Root can't have siblings in postfix form " + s);
              node.nextSibling = new Node (null, null,null);
              node = node.nextSibling;
          }else if (ary[i].equals(")")){
              end ++;
              node = stack.pop();

          } else{
              if (node.name == null) node.name = ary[i];
              else node.name += ary[i];
          }
      }
      return node;
   }

   public String leftParentheticRepresentation() {
       StringBuilder sb = new StringBuilder();
       sb.append(this.name);
       if (this.firstChild != null) {
           sb.append("(");
           sb.append(this.firstChild.leftParentheticRepresentation());
           sb.append(")");
       }

       if (this.nextSibling != null) {
           sb.append(",");
           sb.append(this.nextSibling.leftParentheticRepresentation());
       }
       return sb.toString();
   }


   public static void main (String[] param) {
      String s = "(B1,C)A";
      Node t = Node.parsePostfix (s);
      String v = t.leftParentheticRepresentation();
      System.out.println (s + " ==> " + v); // (B1,C)A ==> A(B1,C)
   }
}

